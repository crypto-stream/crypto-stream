name := "scala-sandbox"

version := "0.1"

scalaVersion := "2.12.4"

val circeVersion = "0.9.3"
val alpakkaVersion = "0.20"
val awScalaVersion = "0.8.+"

lazy val root = project
  .in(file("."))
  .dependsOn(
    core,
    bitFlyerAdapter,
    awsAdapter
  )
  .settings(
    test in assembly := {},
    assemblyJarName in assembly := "crypto-stream.jar",
    assemblyOutputPath in assembly := file("target/crypto-stream.jar")
  )

lazy val core = project
  .in(file("core"))
  .settings(
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-stream" % "2.5.12",
    )
  )

lazy val bitFlyerAdapter = project
  .in(file("bf"))
  .dependsOn(
    core
  )
  .settings(
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http" % "10.1.1",
      "io.circe" %% "circe-core" % circeVersion,
      "io.circe" %% "circe-generic" % circeVersion,
      "io.circe" %% "circe-parser" % circeVersion,
      "com.typesafe" % "config" % "1.3.2"
    )
  )

lazy val awsAdapter = project
  .in(file("aws"))
  .dependsOn(
    core
  )
  .settings(
    libraryDependencies ++= Seq(
      "com.lightbend.akka" %% "akka-stream-alpakka-s3" % alpakkaVersion,
      "com.github.seratch" %% "awscala-dynamodb" % awScalaVersion,
    )
  )

