import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl.{Keep, RestartSink, RestartSource}
import com.github.mulyu.cryptostream.bf.source.BitFlyerSource
import com.github.mulyu.cryptostream.bf.source.WSSubscriber.BFChannel
import com.github.mulyu.cryptostream.dynamo.sink.RateLogSink

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

object TickerToDynamo extends App {

  val decider: Supervision.Decider = _ => Supervision.Restart

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer(
    ActorMaterializerSettings(system)
      .withSupervisionStrategy(decider)
  )

  implicit val ec = ExecutionContext.global
  
  val bitFlyerSource = RestartSource.withBackoff(
    1.second,
    10.second,
    0.1,
    10
  )(() => BitFlyerSource.ticker(BFChannel.TickerFXBTCJPY))

  val dynamoSink = RestartSink.withBackoff(
    1.second,
    10.second,
    0.1,
    10
  )(() => RateLogSink.create)

  bitFlyerSource
    .mapError {
      case error =>
        println(error.printStackTrace())
        error
    }
    .toMat(dynamoSink)(Keep.right)
    .run()
}
