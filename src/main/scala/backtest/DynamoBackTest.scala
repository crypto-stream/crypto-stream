package backtest

import akka.Done
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Broadcast, GraphDSL, RunnableGraph, Sink}
import akka.stream.{ActorMaterializer, ClosedShape}
import com.github.mulyu.cryptostream.dynamo.source.RateLogSource
import com.github.mulyu.cryptostream.flow.RateLog

import scala.concurrent.ExecutionContext
import scala.util.Try

object DynamoBackTest extends App {

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val ec = ExecutionContext.global

  val dynamoSource = RateLogSource.create

  val graph = RunnableGraph.fromGraph(GraphDSL.create() { implicit builder =>
    import GraphDSL.Implicits._

    val bcast = builder.add(Broadcast[RateLog](2))

    dynamoSource ~> bcast
                    bcast ~> Sink.foreach(println)
                    bcast ~> Sink.foreach(println)
    ClosedShape
  })

  Try {
    graph.run()
  }.recover {
    case e =>
      e.printStackTrace()
      Done
  }.map { d =>
    system.terminate()
  }
}
