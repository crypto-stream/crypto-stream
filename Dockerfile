FROM library/openjdk:8-jre
WORKDIR /opt/crypto-stream
ADD target/crypto-stream.jar /opt/crypto-stream/crypto-stream.jar
ENTRYPOINT ["java", "-cp", "crypto-stream.jar"]
