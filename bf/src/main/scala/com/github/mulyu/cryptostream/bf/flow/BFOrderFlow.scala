package com.github.mulyu.cryptostream.bf.flow

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow
import akka.util.ByteString
import com.github.mulyu.cryptostream.bf.model._
import com.github.mulyu.cryptostream.bf.util.{BFConfig, PrivateAPISupport}
import com.github.mulyu.cryptostream.flow.{Buy, Position, Sell}
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._

import scala.concurrent.ExecutionContextExecutor

case class BFOrderRequest(
  product_code: String,
  child_order_type: String,
  side: String,
  price: Double,
  size: Double,
  time_in_force: String
) extends PrivateAPISupport {
  
  def toHttp(config: BFConfig): HttpRequest = {
    val method = HttpMethods.POST
    val uri = Uri("/https://api.bitflyer.jp/v1/me/sendchildorder")
    val body = this.asJson.noSpaces
    HttpRequest(
      method,
      uri,
      privateHeaders(config, method, uri, body).toList,
      HttpEntity(body)
    )
  }
}

object BFOrderRequest {

  def apply(
    position: Position,
    productCode: ProductCode,
    size: Double,
  ): BFOrderRequest = {
    position match {
      case Buy(cs) =>
        BFOrderRequest(
          productCode.value,
          LIMIT.value,
          BUY.value,
          cs.close.value,
          size,
          GTC.value
        )
      case Sell(cs) =>
        BFOrderRequest(
          productCode.value,
          LIMIT.value,
          SELL.value,
          cs.close.value,
          size,
          GTC.value
        )
    }
  }
}

case class BFOrderResponse(
  child_order_acceptance_id: String
)

object BFOrderFlow {
  
  def create(config: BFConfig)(implicit system: ActorSystem): Flow[BFOrderRequest, BFOrderResponse, NotUsed] = {
    implicit val ec: ExecutionContextExecutor = system.dispatcher
    implicit val mat: ActorMaterializer = ActorMaterializer()
    Flow[BFOrderRequest].mapAsync(1) { req =>
      Http().singleRequest(req.toHttp(config))
        .flatMap(r => r.entity.dataBytes.runFold(ByteString.empty)(_ ++ _)
        .map(s => decode[BFOrderResponse](s.utf8String)
          .getOrElse(sys.error("bf order parse error.")))
        )
    }
  }

  def viaPosition(
    config: BFConfig,
    productCode: ProductCode,
    size: Double
  )(implicit system: ActorSystem): Flow[Position, BFOrderResponse, NotUsed] =
    Flow[Position].map(p =>
      BFOrderRequest(p, productCode, size)
    ).via(
      BFOrderFlow.create(config)
    )
}
