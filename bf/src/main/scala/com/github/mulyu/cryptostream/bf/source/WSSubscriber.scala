package com.github.mulyu.cryptostream.bf.source

import akka.actor.ActorRef
import akka.http.scaladsl.model.ws.TextMessage
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._
import io.circe.{Decoder, Encoder, Error}
trait WSSubscriber {

  def start(): Unit

  def close(): Unit
}

private final class WSSubscriberImpl (
  actorRef: ActorRef,
  startMessage: TextMessage,
  closeMessage: TextMessage
) extends WSSubscriber {

  override def start(): Unit = actorRef ! startMessage

  override def close(): Unit = actorRef ! closeMessage
}

object WSSubscriber {

  sealed trait BFChannel {
    def channelName: String
  }

  object BFChannel {

    sealed trait BFTickerChannel extends BFChannel

    case object TickerBTCJPY extends BFTickerChannel {
      override def channelName: String = "lightning_ticker_BTC_JPY"
    }

    case object TickerFXBTCJPY extends BFTickerChannel {
      override def channelName: String = "lightning_ticker_FX_BTC_JPY"
    }

    case object TickerETHBTC extends BFTickerChannel {
      override def channelName: String = "lightning_ticker_ETH_BTC"
    }
  }

  def forBitFlyer(actorRef: ActorRef, channel: BFChannel): WSSubscriber =
    new WSSubscriberImpl(
      actorRef,
      TextMessage(JsonRpcFormat.from("subscribe", BFRequest(channel.channelName)).jsonString),
      TextMessage(JsonRpcFormat.from("unsubscribe", BFRequest(channel.channelName)).jsonString)
    )
}

case class JsonRpcFormat[A](
  method: String,
  params: A
) {

  def jsonString(implicit encoder: Encoder[A]): String = this.asJson.noSpaces
}

object JsonRpcFormat {

  def from[A](
    method: String,
    params: A
  ): JsonRpcFormat[A] =
    JsonRpcFormat(method, params)

  def decodeFrom[A](str: String)(implicit decoder: Decoder[A]): Either[Error, JsonRpcFormat[A]] =
    decode[JsonRpcFormat[A]](str)
}