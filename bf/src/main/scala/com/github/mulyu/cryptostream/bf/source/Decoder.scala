package com.github.mulyu.cryptostream.bf.source

import akka.NotUsed
import akka.http.scaladsl.model.ws.{Message, TextMessage}
import akka.stream.Materializer
import akka.stream.scaladsl.{Flow, Sink}
import io.circe.Decoder
import io.circe.parser._

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object Decoder {

  private val strMergeSink = Sink.fold[String, String]("")(_ + _)

  private def stringToDecoded[A](implicit decoder: Decoder[A]): Flow[String, A, NotUsed] =
    Flow.fromFunction { str =>
      decode[A](str).getOrElse(
        sys.error(s"decode error: $str")
      )
    }

  private def messageToString(
    implicit materializer: Materializer
  ): Flow[Message, String, NotUsed] =
    Flow.fromFunction {
      case TextMessage.Streamed(stream) =>
        Await.result(stream.runWith(strMergeSink), Duration.Inf)
      case TextMessage.Strict(str) =>
        str
      case _ =>
        sys.error("not supported for binary message.")
    }

  def messageDecode[A](
    implicit decoder: Decoder[A],
    materializer: Materializer
  ): Flow[Message, A, NotUsed] =
    messageToString via stringToDecoded
}
