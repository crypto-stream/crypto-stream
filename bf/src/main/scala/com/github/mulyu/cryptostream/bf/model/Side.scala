package com.github.mulyu.cryptostream.bf.model

sealed abstract class Side(
  val value: String
)

case object BUY extends Side("BUY")
case object SELL extends Side("SELL")
