package com.github.mulyu.cryptostream.bf.model

sealed abstract class TimeInForce(
  val value: String
)

case object GTC extends TimeInForce("GTC")
case object IOC extends TimeInForce("IOC")
case object FOK extends TimeInForce("FOK")
