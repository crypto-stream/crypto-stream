package com.github.mulyu.cryptostream.bf.source

import akka.NotUsed
import akka.stream.scaladsl.Source
import akka.stream.stage.{GraphStage, GraphStageLogic, OutHandler, TimerGraphStageLogic}
import akka.stream.{Attributes, Outlet, SourceShape}

import scala.concurrent.duration.FiniteDuration
import scala.math.{abs, random}

class RandomSource(duration: FiniteDuration) extends GraphStage[SourceShape[Double]] {

  private val out: Outlet[Double] = Outlet("RandomSource.out")

  override def shape: SourceShape[Double] = SourceShape(out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic = {
    new TimerGraphStageLogic(shape) {
      setHandler(out, new OutHandler {
        override def onPull(): Unit = ()
      })

      override def preStart(): Unit =
        schedulePeriodically("out", duration)

      override def onTimer(timerKey: Any): Unit = {
        push(out, abs(random()))
      }
    }
  }

}

object RandomSource {

  def apply(duration: FiniteDuration): Source[Double, NotUsed] =
    Source.fromGraph(new RandomSource(duration))
}