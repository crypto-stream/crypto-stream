package com.github.mulyu.cryptostream.bf.model

sealed abstract class ProductCode(
  val value: String
)

case object BTC_JPY extends ProductCode("BTC_JPY")
case object FX_BTC_JPY extends ProductCode("FX_BTC_JPY")
case object ETH_JPY extends ProductCode("ETH_JPY")
