package com.github.mulyu.cryptostream.bf.model

sealed abstract class ChildOrderType(
  val value: String
)

case object LIMIT extends ChildOrderType("LIMIT")
case object MARKET extends ChildOrderType("MARKET")
