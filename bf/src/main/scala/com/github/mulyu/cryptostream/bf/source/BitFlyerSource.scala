package com.github.mulyu.cryptostream.bf.source

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.ws.{Message, WebSocketUpgradeResponse}
import akka.stream.scaladsl.{Flow, Keep, Source}
import akka.stream.{ActorMaterializer, OverflowStrategy}
import com.github.mulyu.cryptostream.bf.source.WSSubscriber.BFChannel.BFTickerChannel
import com.github.mulyu.cryptostream.flow.RateLog
import io.circe.generic.auto._

import scala.concurrent.{ExecutionContext, Future}

object BitFlyerSource {

  // webSocketClientFlow can not re-use.
  private def wsFlow(
    implicit system: ActorSystem
  ): Flow[Message, Message, Future[WebSocketUpgradeResponse]] =
    Http().webSocketClientFlow(
      "wss://ws.lightstream.bitflyer.com/json-rpc"
    )

  def ticker(channel: BFTickerChannel)(
    implicit system: ActorSystem,
    mat: ActorMaterializer
  ): Source[RateLog, Future[WebSocketUpgradeResponse]] = {

    implicit val ec: ExecutionContext = system.dispatcher

    val wsSource: Source[RateLog, (ActorRef, Future[WebSocketUpgradeResponse])] =
      Source.actorRef[Message](bufferSize = 10, OverflowStrategy.fail)
        .viaMat(wsFlow)(Keep.both)
        .mapError {
          case error =>
            println(error.printStackTrace())
            error
        }
        .via(Decoder.messageDecode[JsonRpcFormat[BFResponse[BFTicker]]])
        .map(_.params.message.toRateLog)
    wsSource.mapMaterializedValue {
      case (actorRef, upgraded) =>
        upgraded.foreach(_ =>
          WSSubscriber.forBitFlyer(actorRef, channel).start()
        )
        upgraded
    }
  }
}

case class BFRequest(
  channel: String
)

case class BFResponse[A](
  channel: String,
  message: A
)

case class BFTicker(
  timestamp: String,
  ltp: Double
) {
  
  val toRateLog: RateLog =
    RateLog(ltp, ZonedDateTime.parse(timestamp, DateTimeFormatter.ISO_DATE_TIME).toEpochSecond)
}