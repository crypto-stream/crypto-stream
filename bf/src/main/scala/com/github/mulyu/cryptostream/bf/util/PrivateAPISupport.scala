package com.github.mulyu.cryptostream.bf.util

import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{HttpHeader, HttpMethod, HttpRequest, Uri}
import com.typesafe.config.ConfigFactory
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

trait PrivateAPISupport {

  private def mac(config: BFConfig): Mac = {
    val sha256 = Mac.getInstance("HmacSHA256")
    val secretKey = new SecretKeySpec(config.secretAccessKey.getBytes, "HmacSHA256")
    sha256.init(secretKey)
    sha256
  }
  
  protected def privateHeaders(config: BFConfig, method: HttpMethod, uri: Uri, body: String): Seq[HttpHeader] = {
    val timestamp = System.currentTimeMillis().toString
    val methodStr = method.value
    val path = uri.path.toString()
    val joined = s"$timestamp$methodStr$path$body"
    val sign = mac(config).doFinal(joined.getBytes).map("%02x".format(_)).mkString
    Seq(
      RawHeader("ACCESS-KEY", config.accessKey),
      RawHeader("ACCESS-TIMESTAMP", timestamp),
      RawHeader("ACCESS-SIGN", sign)
    )
  }
}

trait BFConfig {
  def accessKey: String
  def secretAccessKey: String
}

object DefaultBFConfig extends BFConfig {

  private val config = ConfigFactory.load()

  override def accessKey: String = config.getString("bf.accessKey")

  override def secretAccessKey: String = config.getString("bf.accessSecret")
}