package com.github.mulyu.cryptostream.flow

import akka.NotUsed
import akka.stream.scaladsl.Flow
import akka.stream.{Attributes, FlowShape, Inlet, Outlet}
import akka.stream.stage._

import scala.concurrent.duration.FiniteDuration

case class RateLog(
  value: Double,
  timestamp: Long
)

object RateLog {
  val zero = RateLog(0, 0)
}

case class CandleStick(
  open: RateLog,
  close: RateLog,
  high: RateLog,
  low: RateLog
) {

  def update(newV: RateLog): CandleStick = {
    if (this == CandleStick.zeros) {
      CandleStick(newV, newV, newV, newV)
    } else {
      val closeV = newV
      val highV = if (high.value <= newV.value) {
        newV
      } else {
        high
      }
      val lowV = if (low.value >= newV.value) {
        newV
      } else {
        low
      }
      CandleStick(open, closeV, highV, lowV)
    }
  }

  def isOverHigh(target: CandleStick): Boolean =
    high.value < target.close.value

  def isOverLow(target: CandleStick): Boolean =
    low.value > target.close.value

}

object CandleStick {

  val zeros: CandleStick = CandleStick(RateLog.zero, RateLog.zero, RateLog.zero, RateLog.zero)
}

class CandleStickFlow(duration: FiniteDuration) extends GraphStage[FlowShape[RateLog, CandleStick]]{

  private val in: Inlet[RateLog] = Inlet("CandleStickFlow.in")

  private val out: Outlet[CandleStick] = Outlet("CandleStickFlow.out")

  override def shape: FlowShape[RateLog, CandleStick] = FlowShape(in, out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic = {
    new TimerGraphStageLogic(shape) {
      var candleStick: CandleStick = CandleStick.zeros

      object IntervalSchedule
      setHandler(in, new InHandler {
        override def onPush(): Unit = {
          val elem = grab(in)
          candleStick = candleStick.update(elem)
          pull(in)
        }
      })
      setHandler(out, new OutHandler {
        override def onPull(): Unit = ()
      })

      override def preStart(): Unit = {
        pull(in)
        schedulePeriodically(IntervalSchedule, duration)
      }

      override def onTimer(timerKey: Any): Unit = {
        timerKey match {
          case IntervalSchedule =>
            push(out, candleStick)
            candleStick = CandleStick.zeros
          case _ =>
            ()
        }
      }
    }
  }
}

object CandleStickFlow {

  def apply(duration: FiniteDuration): Flow[RateLog, CandleStick, NotUsed] =
    Flow.fromGraph(new CandleStickFlow(duration))
}