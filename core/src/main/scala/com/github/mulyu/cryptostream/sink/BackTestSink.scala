package com.github.mulyu.cryptostream.sink

import akka.NotUsed
import akka.stream.scaladsl.Sink
import akka.stream.stage.{GraphStage, GraphStageLogic, InHandler}
import akka.stream.{Attributes, Inlet, SinkShape}
import com.github.mulyu.cryptostream.flow.{Buy, Neutral, Position, Sell}

class BackTestSink(
  lotSize: Double
) extends GraphStage[SinkShape[Position]] {

  private val in: Inlet[Position] = Inlet("BackTestSink.in")

  override def shape: SinkShape[Position] = SinkShape(in)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new GraphStageLogic(shape) {
      var state: Position = Neutral
      var acc: Double = 0

      override def preStart(): Unit = pull(in)

      setHandler(in, new InHandler {
        override def onPush(): Unit = {
          val elem = grab(in)
          (state, elem) match {
            case (Neutral, e: Buy) =>
              state = e
              println(s"Buy: ${e.candleStick.close}")
            case (Neutral, e: Sell) =>
              state = e
              println(s"Sell: ${e.rate}")
            case (s: Buy, e: Sell) =>
              acc = acc + (e.candleStick.close.value - s.candleStick.close.value) * lotSize
              state = Neutral
              println(s"Close: ${e.candleStick.close}")
            case (s: Sell, e: Buy) =>
              acc = acc + (s.candleStick.close.value - e.candleStick.close.value) * lotSize
              state = Neutral
              println(s"Close: ${e.candleStick.close}")
            case _ =>
              println(s"Keep: $state")
          }
          println(s"Gained: $acc")
          pull(in)
        }
      })
    }
}

object BackTestSink {

  def apply(lotSize: Double): Sink[Position, NotUsed] =
    Sink.fromGraph(new BackTestSink(lotSize))
}