package com.github.mulyu.cryptostream.flow

import akka.NotUsed
import akka.stream.scaladsl.Flow
import akka.stream.stage.{GraphStage, GraphStageLogic, InHandler, OutHandler}
import akka.stream.{Attributes, FlowShape, Inlet, Outlet}

trait Position

case class Buy(candleStick: CandleStick) extends Position {
  val rate: Double = candleStick.close.value
}

case class Sell(candleStick: CandleStick) extends Position {
  val rate: Double = candleStick.close.value
}

case object Neutral extends Position

case class BreakOutInterval(
  values: Seq[CandleStick],
  limit: Int
) {
  require(values.size <= limit)

  private val isFilled: Boolean = values.size == limit

  def append(value: CandleStick): BreakOutInterval =
    if (isFilled) {
      BreakOutInterval(values.tail :+ value, limit)
    } else {
      BreakOutInterval(values :+ value, limit)
    }

  def isBreakOutHigh(value: CandleStick): Boolean =
    isFilled && values.maxBy(_.high.value).isOverHigh(value)

  def isBreakOutLow(value: CandleStick): Boolean =
    isFilled && values.minBy(_.low.value).isOverLow(value)
}

object BreakOutInterval {

  def empty[A](limit: Int): BreakOutInterval = BreakOutInterval(Seq.empty, limit)
}


class DonchianChannelBreakOutFlow(
  intervalSize: Int
) extends GraphStage[FlowShape[CandleStick, Position]] {

  private val in: Inlet[CandleStick] = Inlet("DonchianChannelBreakOutFlow.in")

  private val out: Outlet[Position] = Outlet("DonchianChannelBreakOutFlow.out")

  override def shape: FlowShape[CandleStick, Position] = FlowShape(in, out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new GraphStageLogic(shape) {
      var acc: BreakOutInterval = BreakOutInterval.empty(intervalSize)
      var state: Position = Neutral

      override def preStart(): Unit = pull(in)

      setHandler(in, new InHandler {
        override def onPush(): Unit = {
          val elem = grab(in)

          (acc.isBreakOutHigh(elem), acc.isBreakOutLow(elem)) match {
            case (true, _) =>
              state = Buy(elem)
              push(out, Buy(elem))
            case (_, true) =>
              state = Sell(elem)
              push(out, Sell(elem))
            case _ =>
              state = Neutral
              push(out, Neutral)
          }
          acc = acc.append(elem)
          pull(in)
        }
      })

      setHandler(out, new OutHandler {
        override def onPull(): Unit = ()
      })
    }
}

object DonchianChannelBreakOutFlow {

  def apply(intervalSize: Int): Flow[CandleStick, Position, NotUsed] =
    Flow.fromGraph(new DonchianChannelBreakOutFlow(intervalSize))
}
