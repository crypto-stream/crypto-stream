package com.github.mulyu.cryptostream.dynamo.source

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.github.mulyu.cryptostream.dynamo.sink.RateLogRecordKey
import com.github.mulyu.cryptostream.flow.RateLog
import awscala.dynamodbv2._

object RateLogSource {

  def create: Source[RateLog, NotUsed] =
    Source.fromIterator { () =>
      import com.github.mulyu.cryptostream.dynamo.DynamoDBProvider._
      val items = rateLog.scan(Seq(
        RateLogRecordKey.keyName -> cond.isNotNull
      ))
      val values = items.flatMap(RateLogFactory.fromItem).sortBy(_.timestamp)
      values.iterator
    }
}

private[source] object RateLogFactory {

  def fromItem(item: Item): Option[RateLog] = {
    val attributes = item.attributes
    for {
      timestampAttr <- attributes.find(_.name == "timestamp").map(_.value)
      valueAttr <- attributes.find(_.name == "value").map(_.value)
      timestamp <- timestampAttr.n.map(_.toLong)
      value <- valueAttr.n.map(_.toDouble)
    } yield RateLog(value, timestamp)
  }

}
