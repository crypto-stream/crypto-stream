package com.github.mulyu.cryptostream.dynamo

import awscala.dynamodbv2.{DynamoDB, Table}
import com.amazonaws.regions.{Region, Regions}

private[dynamo] object DynamoDBProvider {

  implicit val dynamodb: DynamoDB = DynamoDB.at(Region.getRegion(Regions.AP_NORTHEAST_1))

  val rateLog: Table = dynamodb.table("rate_log").getOrElse(
    throw new RuntimeException("rate_log dynamo table is not exist.")
  )
}
