package com.github.mulyu.cryptostream.dynamo.sink

import akka.NotUsed
import akka.stream.scaladsl.Sink
import akka.stream.stage.{GraphStage, GraphStageLogic, InHandler}
import akka.stream.{Attributes, Inlet, SinkShape}
import com.github.mulyu.cryptostream.flow.RateLog

import scala.util.Try

class RateLogSink extends GraphStage[SinkShape[RateLog]]{

  import com.github.mulyu.cryptostream.dynamo.DynamoDBProvider._

  val inlet: Inlet[RateLog] = Inlet("RateLogSink.in")

  override def shape: SinkShape[RateLog] = SinkShape(inlet)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new GraphStageLogic(shape) {

      override def preStart(): Unit = {
        pull(inlet)
      }

      setHandler(inlet, new InHandler {
        override def onPush(): Unit = {
          val elem = grab(inlet)
          val record = RateLogRecord(elem)
          Try {
            rateLog.putItem(
              record.key.value,
              record.values: _*
            )
          }.recover {
            case e =>
              e.printStackTrace()
              throw e
          }.map{ _ =>
            pull(inlet)
          }
        }
      })
    }

}

object RateLogSink {

  def create: Sink[RateLog, NotUsed] = Sink.fromGraph(new RateLogSink())
}

case class RateLogRecordKey(
  _value: Double,
  timestamp: Long
) {
  val value: String = s"${timestamp}_${_value}"
}

object RateLogRecordKey {

  val keyName: String = "time_rate"

  def apply(v: RateLog): RateLogRecordKey = RateLogRecordKey(v.value, v.timestamp)
}

case class RateLogRecord(
  key: RateLogRecordKey,
  v: RateLog
) {

  val values: Seq[(String, Any)] =
    Seq(
      "value" -> v.value,
      "timestamp" -> v.timestamp,
      "ttl" -> (v.timestamp + (60 * 60 * 24).toLong)
    )
}

object RateLogRecord {

  def apply(v: RateLog): RateLogRecord = RateLogRecord(RateLogRecordKey(v), v)
}
