package com.github.mulyu.cryptostream.s3.sink

import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit

import akka.actor.ActorSystem
import akka.stream.alpakka.s3.S3Settings
import akka.stream.alpakka.s3.scaladsl.{MultipartUploadResult, S3Client}
import akka.stream.scaladsl.{Sink, Source}
import akka.stream.{Attributes, Inlet, Materializer, SinkShape}
import akka.stream.stage.{GraphStage, GraphStageLogic, InHandler}
import akka.util.ByteString

import scala.concurrent.Future


class DynamicS3Sink(
   partitionBy      : ChronoUnit
)(implicit val actorSystem: ActorSystem, materializer: Materializer) extends GraphStage[SinkShape[ByteString]] {

  private val inlet: Inlet[ByteString] = Inlet("DynamicS3Sink.in")

  private val s3Client = new S3Client(S3Settings())

  override def shape: SinkShape[ByteString] = SinkShape(inlet)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new GraphStageLogic(shape) {
      var subSource: SubSourceOutlet[ByteString] = createSubSource
      var partitionStart: ZonedDateTime = ZonedDateTime.now().truncatedTo(partitionBy)

      private def createSubSource: SubSourceOutlet[ByteString] = {
        val subSource = new SubSourceOutlet[ByteString]("SubSource.out")
        subSource.setHandler(() => {
          if (isClosed(inlet)) {
            completeStage()
          } else if (isNextPartition) {
            sinkRestart()
          } else {
            pull(inlet)
          }
        })
        subSource
      }

      private def createS3Sink: Sink[ByteString, Future[MultipartUploadResult]] =
        s3Client.multipartUpload("bt-candle", s"candle_${partitionStart.toEpochSecond.toString}.csv")

      private def isNextPartition: Boolean = {
        val now = ZonedDateTime.now().truncatedTo(partitionBy)
        now.isAfter(partitionStart.truncatedTo(partitionBy))
      }

      private def sinkRestart(): Unit = {
        subSource.complete()

        subSource = createSubSource
        partitionStart = ZonedDateTime.now().truncatedTo(partitionBy)

        Source.fromGraph(
          subSource.source
        ).to(
          createS3Sink
        ).run()(subFusingMaterializer)
      }

      override def preStart(): Unit = {
        sinkRestart()
      }

      setHandler(inlet, new InHandler {
        override def onPush(): Unit = {
          val elem = grab(inlet)
          subSource.push(elem)
        }
      })

    }
}
